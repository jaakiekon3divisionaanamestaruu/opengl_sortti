package sortti_opengl_ehka_jopa_toimiva;
import static com.jogamp.opengl.GL.*;
import com.jogamp.opengl.GL2;
import static com.jogamp.opengl.GL2ES1.GL_PERSPECTIVE_CORRECTION_HINT;
import static com.jogamp.opengl.GL2ES3.GL_QUADS;
import com.jogamp.opengl.GLAutoDrawable;
import com.jogamp.opengl.GLEventListener;
import com.jogamp.opengl.awt.GLCanvas;
import static com.jogamp.opengl.fixedfunc.GLLightingFunc.GL_SMOOTH;
import static com.jogamp.opengl.fixedfunc.GLMatrixFunc.GL_MODELVIEW;
import static com.jogamp.opengl.fixedfunc.GLMatrixFunc.GL_PROJECTION;
import com.jogamp.opengl.glu.GLU;


import java.util.Random;
import sorting.Sorttaus;


 
/**
 * JOGL 2.0 Program Template (GLCanvas)
 * This is a "Component" which can be added into a top-level "Container".
 * It also handles the OpenGL events to render graphics.
 */
@SuppressWarnings("serial")
public class Renderer extends GLCanvas implements GLEventListener {
   private GLU glu;  // for the GL Utility
   public int ELEMENTS = 10;
   public float[] lista = new float[ELEMENTS];
   public float[] mergelista = null;
   
   public void random(int k) {
       ELEMENTS = k;
       lista = new float[ELEMENTS];  
       if(mergelista != null) {
           mergelista = new float[ELEMENTS];
       }
       Random s = new Random();
       for (int i  = 0; i < ELEMENTS; i++) {
           lista[i] = 2f * s.nextFloat();
       }
   }
   public void sort(String algo) {
       if(algo.contains("insertionsort")) {
           long start = System.nanoTime();
           Sorttaus.insertionsort(lista);
           System.out.println((System.nanoTime() - start)/1000);
       }
       else if(algo.contains("quicksort")) {
           long start = System.nanoTime();
           Sorttaus.quicksort(lista, 0, lista.length);
           System.out.println((System.nanoTime() - start)/1000);
       }
       else if(algo.contains("quickinsert")) {
           long start = System.nanoTime();
           Sorttaus.quickinsert(lista, 0, lista.length);
           Sorttaus.insertionsort(lista);
           System.out.println((System.nanoTime() - start)/1000);
       }
       else if(algo.contains("cocktailshakersort")) {
           long start = System.nanoTime();
           Sorttaus.cocktailsort(lista);
           System.out.println((System.nanoTime() - start)/1000);
       }
       else if(algo.contains("heapsort")) {
           long start = System.nanoTime();
           Sorttaus.heapsort(lista);
           System.out.println((System.nanoTime() - start)/1000);
       }
       else if(algo.contains("slowsort")) {
           long start = System.nanoTime();
           Sorttaus.slowsort(lista, 0, lista.length-1);
           System.out.println((System.nanoTime() - start)/1000);
       }
       else if(algo.contains("bogosort")) {
           long start = System.nanoTime();
           Sorttaus.bogosort(lista);
           System.out.println((System.nanoTime() - start)/1000);
       }
       else if(algo.contains("mergesort")) {
           long start = System.nanoTime();
           mergelista = new float[ELEMENTS];
           Sorttaus.mergesort(lista, mergelista, lista.length);
           System.out.println((System.nanoTime() - start)/1000);
       }
       Sorttaus.lastComparison[0] = -1;
       Sorttaus.lastComparison[1] = -1;
       Sorttaus.lastSwap[0] = -1;
       Sorttaus.lastSwap[1] = -1;
   }
   
   /** Constructor to setup the GUI for this Component */
   public Renderer() {
      this.addGLEventListener(this);
   }
   
 
   // ------ Implement methods declared in GLEventListener ------
 
   /**
    * Called back immediately after the OpenGL context is initialized. Can be used
    * to perform one-time initialization. Run only once.
    */
   @Override
   public void init(GLAutoDrawable drawable) {
      GL2 gl = drawable.getGL().getGL2();      // get the OpenGL graphics context
      glu = new GLU();                         // get GL Utilities
      gl.glClearColor(255.0f, 255.0f, 255.0f, 0.0f); // set background (clear) color
      gl.glClearDepth(1.0f);      // set clear depth value to farthest
      gl.glEnable(GL_DEPTH_TEST); // enables depth testing
      gl.glDepthFunc(GL_LEQUAL);  // the type of depth test to do
      gl.glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST); // best perspective correction
      gl.glShadeModel(GL_SMOOTH); // blends colors nicely, and smoothes out lighting
 
      // ----- Your OpenGL initialization code here -----
   }
 
   /**
    * Call-back handler for window re-size event. Also called when the drawable is
    * first set to visible.
    */
   @Override
   public void reshape(GLAutoDrawable drawable, int x, int y, int width, int height) {
      GL2 gl = drawable.getGL().getGL2();  // get the OpenGL 2 graphics context
 
      if (height == 0) height = 1;   // prevent divide by zero
      float aspect = (float)width / height;
 
      // Set the view port (display area) to cover the entire window
      gl.glViewport(0, 0, width, height);
 
      // Setup perspective projection, with aspect ratio matches viewport
      gl.glMatrixMode(GL_PROJECTION);  // choose projection matrix
      gl.glLoadIdentity();             // reset projection matrix
      //glu.gluPerspective(45.0, aspect, 0.1, 100.0); // fovy, aspect, zNear, zFar
 
      // Enable the model-view transform
      gl.glMatrixMode(GL_MODELVIEW);
      gl.glLoadIdentity(); // reset
   }
 
   /**
    * Called back by the animator to perform rendering.
    */
   @Override
   public void display(GLAutoDrawable drawable) {
      GL2 gl = drawable.getGL().getGL2();  // get the OpenGL 2 graphics context
      gl.glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT); // clear color and depth buffers
      gl.glLoadIdentity();  // reset the model-view matrix
 
      // ----- Your OpenGL rendering code here (render a white triangle for testing) -----
      gl.glTranslatef(-1f, -1f, 0f); // translate into the screen
      gl.glBegin(GL_QUADS);
        gl.glColor3f(0f, 0f, 0f);
        float q = 2f / ELEMENTS;
        int[] swap = Sorttaus.lastSwap;
        int[] comp = Sorttaus.lastComparison;
        for(int i = 0; i < ELEMENTS; i++) {
            if(mergelista == null) {
                if(i == swap[0] || i == swap[1]) {
                    gl.glColor3f(1f,0f, 0f);
                }
                if(i == comp[0] || i == comp[1]) {
                    gl.glColor3f(0f, 0f, 1f);
                }
                gl.glVertex2f(i * q, lista[i]);
                gl.glVertex2f(i * q, 0);
                gl.glVertex2f((i + 1) * q, 0);
                gl.glVertex2f((i + 1) * q, lista[i]);
                gl.glColor3f(0f, 0f, 0f);
            }
            else {
                
                if((i == comp[0] || i == comp[1]) && Sorttaus.mergeSwapArrays) {
                    gl.glColor3f(0f, 0f, 1f);
                }
                if(i == swap[0] && !Sorttaus.mergeSwapArrays) {
                    gl.glColor3f(1f, 0f, 0f);
                }
                gl.glVertex2f(i * q, (lista[i]/2));
                gl.glVertex2f(i * q, 0);
                gl.glVertex2f((i + 1) * q, 0);
                gl.glVertex2f((i + 1) * q, lista[i]/2);
                gl.glColor3f(0f, 0f, 0f);
                
                if((i == comp[0] || i == comp[1]) && !Sorttaus.mergeSwapArrays) {
                    gl.glColor3f(0f, 0f, 1f);
                }
                if(i == swap[0] && Sorttaus.mergeSwapArrays) {
                    gl.glColor3f(1f, 0f, 0f);
                }
                
                gl.glVertex2f(i * q, 1 +mergelista[i]/2);
                gl.glVertex2f(i * q, 1);
                gl.glVertex2f((i + 1) * q, 1);
                gl.glVertex2f((i + 1) * q, 1 + mergelista[i]/2);
                gl.glColor3f(0f, 0f, 0f);
            }
        }
      gl.glEnd();
   }
 
   /**
    * Called back before the OpenGL context is destroyed. Release resource such as buffers.
    */
   @Override
   public void dispose(GLAutoDrawable drawable) { }
}