package sortti_opengl_ehka_jopa_toimiva;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import com.jogamp.opengl.util.FPSAnimator;
import sorting.Sorttaus;


@SuppressWarnings("serial")
public class MainWindow extends JFrame {
    // Define constants for the top-level container

    private static String TITLE = "SORTTI_OPENGL_EHKA_JOPA_TOIMIVA";  // window's title
    private static final int CANVAS_WIDTH = 1200;  // width of the drawable
    private static final int CANVAS_HEIGHT = 600; // height of the drawable
    private static final int FPS = 144; // animator's target frames per second
    
    String[] algos = new String[] {"bogosort", "cocktailshakersort", "heapsort", "insertionsort", "mergesort", "quickinsert", "quicksort", "slowsort"};
    
    private final Renderer canvas;
    private final JTextField elements;
    private final JComboBox algorithm;
    JTextField comp, swap;
    
    Thread sort = new Thread();

    /**
     * Constructor to setup the top-level container and animator
     */
    private void random() {
        if(!sort.isAlive()) {
            canvas.random(Integer.parseInt(elements.getText()));
        }
    }
    private void stop() {
        if(sort.isAlive()) {
            sort.stop();
        }
    }
    private void algoAction() {
        stop();
        while(sort.isAlive()) {
            try {
                Thread.sleep(400);
            }
            catch(Exception e) {
                
            }
        }
        if(algorithm.getSelectedItem().toString().contains("mergesort")) {
            canvas.mergelista = new float[canvas.ELEMENTS];
        }
        else {
            canvas.mergelista = null;
        }
    }

    private void sort() {
        if(!sort.isAlive()) {
            sort = new Thread(
                new Runnable() {
                    public void run() {
                        Sorttaus.changeSleepC(Float.parseFloat(comp.getText()));
                        Sorttaus.changeSleepS(Float.parseFloat(swap.getText()));
                        canvas.sort(algorithm.getSelectedItem().toString());
                    }
                }
            );
            sort.start();
        }

    }

    public MainWindow() {
        // Create the OpenGL rendering canvas
        canvas = new Renderer();
        canvas.setPreferredSize(new Dimension(CANVAS_WIDTH, CANVAS_HEIGHT));
        JPanel pane = new JPanel();
        pane.setBorder(BorderFactory.createLineBorder(Color.WHITE, 1));
        pane.add(canvas);
        // Create a animator that drives canvas' display() at the specified FPS.
        final FPSAnimator animator = new FPSAnimator(canvas, FPS, true);

        // Create the top-level container frame
        this.setLayout(new BorderLayout());
        this.getContentPane().add(pane, BorderLayout.CENTER);
        JPanel buttons = new JPanel();
        buttons.setLayout(new FlowLayout());
        
        JLabel desc = new JLabel("N:");
        buttons.add(desc);
        elements = new JTextField("10");
        elements.setPreferredSize(new Dimension(50, 25));
        buttons.add(elements);
        
        JButton random = new JButton("Random");
        random.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                random();
            }

        });
        buttons.add(random);

        JButton sort = new JButton("Sort");
        sort.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                sort();
            }

        });
        buttons.add(sort);
        
        
        JLabel desc1 = new JLabel("Comparison sleep (μs):");
        buttons.add(desc1);
        comp  = new JTextField("1000");
        comp.setPreferredSize(new Dimension(70, 25));
        buttons.add(comp);
        
        JLabel desc2 = new JLabel("Swap sleep (μs):");
        buttons.add(desc2);
        swap  = new JTextField("1000");
        swap.setPreferredSize(new Dimension(70, 25));
        buttons.add(swap);
        
        

        this.getContentPane().add(buttons, BorderLayout.NORTH);
        
        //BOTTOM SIDE
        JPanel kek = new JPanel();
        kek.setLayout(new FlowLayout());
        algorithm = new JComboBox(algos);
        algorithm.setPreferredSize(new Dimension(200, 30));
        algorithm.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                algoAction();
            }            
        });
        JLabel desc3 = new JLabel("Sort:");
        kek.add(desc3);
        kek.add(algorithm);
        JButton stop = new JButton("STOP SORTING");
        stop.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                stop();
            }
        });
        kek.add(stop);
        
        
        
        this.getContentPane().add(kek, BorderLayout.SOUTH);
        

        this.addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent e) {
                // Use a dedicate thread to run the stop() to ensure that the
                // animator stops before program exits.
                new Thread() {
                    @Override
                    public void run() {
                        if (animator.isStarted()) {
                            animator.stop();
                        }
                        System.exit(0);
                    }
                }.start();
            }
        });
        this.setTitle(TITLE);
        this.pack();
        this.setVisible(true);
        canvas.random(10);
        animator.start(); // start the animation loop
    }

    /**
     * The entry main() method
     */
    public static void main(String[] args) {
        // Run the GUI codes in the event-dispatching thread for thread safety
        SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                new MainWindow(); // run the constructor
            }
        });
    }
}
