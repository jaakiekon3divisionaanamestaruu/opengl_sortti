/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sorting;

import java.util.Random;

/**
 *
 * @author Jaakko
 */
public class Sorttaus {
    private static float comparison_sleep = 1000f;
    private static float swap_sleep = 1000f;
    public static int[] lastSwap = {-1, -1};
    public static int[] lastComparison = {-1, -1};
    public static boolean mergeSwapArrays = true;
    public static int mergeCounter = 0;
    private static final Random rand = new Random();
    private static void busySleep(float microseconds) {
        long start = System.nanoTime();
        while (System.nanoTime() - start < microseconds * 1000) {
            
        }
    }
    public static void reset() {
        
        lastSwap[0] = -1;
        lastSwap[1] = -1;
        lastComparison[0] = -1;
        lastComparison[1] = -1;
    }
    public static void changeSleepC(float f1) {
        comparison_sleep = f1;
    }
    public static void changeSleepS(float f1) {
        swap_sleep = f1;
    }
    private static void swap(float[] list, int i1, int i2) {
        float temp = list[i1];
        list[i1] = list[i2];
        list[i2] = temp;
        lastSwap[0] = i1;
        lastSwap[1] = i2;
        lastComparison[0] = -1;
        lastComparison[1] = -1;
        busySleep(swap_sleep);     
    }
    
    private static boolean lessThan(float[] list, int i1, int i2, boolean equal) {
        lastComparison[0] = i1;
        lastComparison[1] = i2;
        lastSwap[0] = -1;
        lastSwap[1] = -1;
        busySleep(comparison_sleep);
        if(equal) {
            return list[i1] <= list[i2];
        }
        return list[i1] < list[i2];
    }
    
    private static boolean biggerThan(float[] list, int i1, int i2, boolean equal) {
        lastComparison[0] = i1;
        lastComparison[1] = i2;
        lastSwap[0] = -1;
        lastSwap[1] = -1;
        busySleep(comparison_sleep);
        if(equal) {
            return list[i1] >= list[i2];
        }
        return list[i1] > list[i2];
    }
    
    
    public static void insertionsort(float[] list) {
        int i = 1;
        while (i < list.length) {
            int j = i;
            while (j>0 && biggerThan(list, j-1, j, false)) {
                swap(list, j, j-1);
                j--;
            }
            i++;
        }
    }
    
    private static int qP(float[] list, int left, int right) {
        int i  = left;
        int j;
        int pivot = left + rand.nextInt(right-left);
        swap(list, left, pivot);
        for (j = left+1; j<right;j++) {
            if(lessThan(list, j, left, true)) {
                i++;
                swap(list, i, j);      
            }
        }
        swap(list, i, left);     
        return i;
    }
    public static void quicksort(float [] list, int left, int right) {
        if(left < right) {            

            int partition = qP(list, left, right);
            quicksort(list, left, partition);
            quicksort(list, partition+1, right);
        }
    }
    
    public static void quickinsert(float[] list, int left, int right) {
         if(left < right) {            
            if((right - left) < 10) {
                return;
            }
            int partition = qP(list, left, right);
            quickinsert(list, left, partition);
            quickinsert(list, partition+1, right);
        }
        
    }
    
    public static void cocktailsort(float[] list) {
        int begin = 0;
        int end = list.length - 2;
        while (begin <= end) {
            int newBegin = end;
            int newEnd = begin;
            for (int i = begin; i < end+1; i++) {
                if (biggerThan(list, i, i+1, false)) {
                    swap(list, i, i+1);
                    newEnd = i;
                }
            }
            end = newEnd;
            for(int i = end-1; i > begin -1;i--) {
                if(biggerThan(list, i, i+1, false)) {
                    swap(list, i, i+1);
                    newBegin = i;
                }
            }
            begin = newBegin;
        }
    }
    
    public static void heapsort(float[] list) {
        int length = list.length;
        buildMaxHeap(list, length);
        for(int i = length - 1; i > 0; i--) {
            swap(list, 0, i);
            maxHeapify(list, 1, i);
        }
    }
    private static void buildMaxHeap(float[] list, int heapSize) {
        if(heapSize > list.length) {
            heapSize = list.length;
        }
        for(int i = heapSize/2; i > 0; i--) {
            maxHeapify(list, i, heapSize);
        }
    }
     private static void maxHeapify(float[] list, int index, int heapSize) {
        int l = index * 2;
        int r = l + 1;
        int largest;
        if(l <= heapSize && biggerThan(list, l - 1, index - 1, false)) {
            largest = l;
        } else {
            largest = index;
        }
        if(r <= heapSize && biggerThan(list, r - 1, largest - 1, false)) {
            largest = r;
        }
        if(largest != index) {
            swap(list, index - 1, largest - 1);
            maxHeapify(list, largest, heapSize);
        }
    }   
     
    public static void slowsort(float[] list, int i, int j) {
        if(i >= j) {
            return;
        }
        int m = (i+j)/2;
        slowsort(list, i, m);
        slowsort(list, m+1, j);
        if (lessThan(list, j, m, false)) {
            swap(list, j, m);
        }
        slowsort(list, i, j-1);
    }    
    public static void bogosort(float[] list) {
        while(!donebogo(list)) {
            shuffle(list);
        }
    }
    private static boolean donebogo(float[] list) {
        boolean done = true;
        for(int i = 0; i < list.length - 1; i++) {
            if(biggerThan(list, i, i+1, false)) {
                done = false;
                break;
            }
        }
        return done;
    }
    
    private static Random random;
    private static void shuffle(float[] array) {
        if (random == null) random = new Random();
        int count = array.length;
        for (int i = count; i > 1; i--) {
            swap(array, i - 1, random.nextInt(i));
        }
    }
    
    public static void mergesort(float[] A, float [] B, int n) {
        mergeSwapArrays = true;
        copyarray(A, 0, n, B);
        mergeSwapArrays = !mergeSwapArrays;
        topdownsplitmerge(B, 0, n, A);
    }
    private static void topdownsplitmerge(float[] B, int begin, int end, float[] A) {
        if(end - begin < 2) {
            return;
        }
        mergeSwapArrays = !mergeSwapArrays;
        int middle = (end + begin) / 2;
        topdownsplitmerge(A, begin, middle, B);
        topdownsplitmerge(A, middle, end, B);
        topdownmerge(B, begin, middle, end, A);
    }
    private static void topdownmerge(float[] A, int begin, int middle, int end, float[] B) {
        int i = begin;
        int j  = middle;
        mergeSwapArrays = !mergeSwapArrays;
        for (int k = begin; k < end; k++) {
            if (i < middle && (j >= end || lessThan(A, i, j, true))) {
                B[k]  = A[i];
                i++;
            } else {
                B[k] = A[j];
                j++;
            }
            lastSwap[0] = k;
            lastSwap[1] = k;
            busySleep(swap_sleep);  
        }
    }
    private static void copyarray(float[] A, int begin, int end, float[] B) {
        for (int i = begin; i < end; i++) {
            B[i] = A[i];
            lastSwap[0] = i;
            lastSwap[1] = i;
            busySleep(swap_sleep);  
        }
    }
}
